package com.altf4.mentalhealth;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.app.Notification;
import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.altf4.mentalhealth.db.PostObject;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Toolbar mainToolbar;
    private static final String TAG = "MainActivity";
    ArrayList<PostObject> posts = new ArrayList<>();
    private BottomNavigationView mainBottomNav;
    StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    FirebaseFirestore fireBaseFirestore = FirebaseFirestore.getInstance();
    PostAdapter postAdapter;
    ArrayList<Comment> commentArrayList;
    RecyclerView post_RV;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mainToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(mainToolbar);
        getSupportActionBar().setTitle("Chat");


        mainBottomNav = findViewById(R.id.mainBottomNav);
        Comment comment = new Comment("my comment");
        ArrayList<Comment> comments = new ArrayList<>();
        comments.add(comment);
        Post post = new Post("my post", "hello");
        //posts.add(post);


        RecyclerView recyclerView = findViewById(R.id.post_RV);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(postAdapter);
        setUpDataBase();

        post_RV = findViewById(R.id.post_RV);
        commentArrayList = new ArrayList<>();

        postAdapter = new PostAdapter(this,posts,commentArrayList);
        post_RV.setLayoutManager(linearLayoutManager);
        post_RV.setAdapter(postAdapter);

        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem item = menu.findItem(R.id.action_add_post);
        item.setChecked(false);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem checkable = menu.findItem(R.id.action_add_post);
        checkable.setChecked(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add_post:
                //change later to whatever our add post class is
                startActivity(new Intent(this, AddPost.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




/*
    //basic read and write for database
    public void basicReadWrite() {
        //Start of writing message
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");

        myRef.setValue("Ni hao!");

        //start of reading message
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String value = snapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());

            }
        });
    }
*/

    public void setUpDataBase () {

        fireBaseFirestore.collection("Posts")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot documents : task.getResult()) {
                                //posts.add((PostObject) document.toObject(PostObject.class));
                                //for (Map.Entry<String, Object> document : documents.getData().entrySet()) {
                                //    posts.add((PostObject) document.getValue());
                                //}
                                Log.d("test", documents.getId());
                                PostObject postObject = new PostObject(documents.getString("id"), documents.getString("post"), documents.getString("createdDate"));
                                posts.add(postObject);
                                postAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }



}