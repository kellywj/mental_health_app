package com.altf4.mentalhealth;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altf4.mentalhealth.db.PostObject;

import java.util.ArrayList;
import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {

   private Activity activity;
   ArrayList<PostObject> postArrayList;
   ArrayList<Comment> commentArrayList;

    public PostAdapter(Activity activity, ArrayList<PostObject> postArrayList, ArrayList<Comment> commentArrayList) {
        this.activity = activity;
        this.postArrayList = postArrayList;
        this.commentArrayList = commentArrayList;
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_post,
                parent, false);

        return new PostViewHolder (view);

    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {

        PostObject post = postArrayList.get(position);

        holder.PostTitle.setText("Anonymous");
        holder.PostBody.setText(post.getPost());
        int time;
        holder.CreatedTime.setText(post.getCreatedDate());


        CommentAdapter commentAdapter = new CommentAdapter(commentArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        holder.NestedRecyclerView.setLayoutManager(linearLayoutManager);
        holder.NestedRecyclerView.setAdapter(commentAdapter);


    }


    @Override
    public int getItemCount() {
        return postArrayList.size();
    }

    class PostViewHolder extends RecyclerView.ViewHolder {

        TextView PostTitle;
        TextView PostBody;
        RecyclerView NestedRecyclerView;
        TextView CreatedTime;

        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            PostTitle = itemView.findViewById(R.id.title_text_view);
            PostBody = itemView.findViewById(R.id.body_text_view);
            NestedRecyclerView = itemView.findViewById(R.id.comment_RV);
            CreatedTime = itemView.findViewById(R.id.created_time);

        }
    }

}
