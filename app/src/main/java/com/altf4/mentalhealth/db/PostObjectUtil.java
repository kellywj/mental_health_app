package com.altf4.mentalhealth.db;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class PostObjectUtil {

    public static List<PostObject> getAllData() {
        List<PostObject> plist = new ArrayList<>();

        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        FirebaseFirestore fireBaseFirestore = FirebaseFirestore.getInstance();

        fireBaseFirestore.collection("Posts")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                               plist.add((PostObject) document.getData());
                            }
                        }
                    }
                });

        return plist;
    }
}
