package com.altf4.mentalhealth.db;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.FieldValue;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PostObject implements Serializable {

    String post;
    String id;
    String createdDate;

    public PostObject(String id, String post, String createdDate) {
        this.post = post;
        this.id = id;
        this.createdDate = createdDate;


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("post", post);
        result.put("createdDate", createdDate);


        return result;
    }



}
