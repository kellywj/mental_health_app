package com.altf4.mentalhealth;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.app.Notification;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.altf4.mentalhealth.db.PostObject;
import com.altf4.mentalhealth.util.RandomStringUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.errorprone.annotations.Var;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.Calendar;


public class AddPost extends AppCompatActivity {

    private static final int MAX_LENGTH = 100;
    private Toolbar newPostToolbar;
    private EditText newPost;
    private Button newPostBtn;
    private StorageReference storageReference;
    private FirebaseFirestore fireBaseFirestore;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);

        storageReference = FirebaseStorage.getInstance().getReference();
        fireBaseFirestore = FirebaseFirestore.getInstance();

        newPostToolbar = (Toolbar) findViewById(R.id.new_post_toolbar);
        setSupportActionBar(newPostToolbar);
        getSupportActionBar().setTitle("Add New Post");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        newPost = findViewById(R.id.post_input);
        newPostBtn = findViewById(R.id.post_button);

        ImageButton imageButton1 = (ImageButton) findViewById(R.id.highschool);
        ImageButton imageButton2 = (ImageButton) findViewById(R.id.depression);
        ImageButton imageButton3 = (ImageButton) findViewById(R.id.relationships);
        ImageButton imageButton4 = (ImageButton) findViewById(R.id.checkbox);
        ImageView imageView = findViewById(R.id.imageView);
        imageButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setImageDrawable(getDrawable(R.drawable.group_6__1_));
            }
        });

        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setImageDrawable(getDrawable(R.drawable.group_28));
            }
        });

        imageButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setImageDrawable(getDrawable(R.drawable.group_29));
            }
        });

        imageButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setImageDrawable(getDrawable(R.drawable.checked_check_box__1_));
            }
        });



        newPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String post = newPost.getText().toString();
                String id = RandomStringUtil.random();

                if (!TextUtils.isEmpty(post)) {


                    Map<String, Object> postMap = new HashMap<>();

                    Date currentTime = Calendar.getInstance().getTime();
                    SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM yyyy hh:mm a");
                    String time = dfDate.format(currentTime);
                    System.out.println(time);
                    PostObject postObject = new PostObject(RandomStringUtil.random(), post, time);


/*
                    fireBaseFirestore.collection("Post")
                            .add(postMap)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error adding document", e);
                                }
                            });

*/
                    //Adding the post into the database
                    fireBaseFirestore.collection("Posts").add(postObject.toMap()).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentReference> task) {

                            if (task.isSuccessful()) {

                                Toast.makeText(AddPost.this, "Post was added", Toast.LENGTH_LONG).show();
                                int tem = task.hashCode();
                                Intent mainIntent = new Intent(AddPost.this, MainActivity.class);
                                startActivity(mainIntent);
                                finish();
                            }


                        }
                    });

                }


            }
        });


    }




}